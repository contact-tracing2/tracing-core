package de.uniba.akshaychoudhry.tracingcore

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class TracingCoreApplication

fun main(args: Array<String>) {
	runApplication<TracingCoreApplication>(*args)
}
