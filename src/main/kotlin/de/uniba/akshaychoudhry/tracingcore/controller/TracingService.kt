package de.uniba.akshaychoudhry.tracingcore.controller

import de.uniba.akshaychoudhry.tracingcore.dao.entity.*
import de.uniba.akshaychoudhry.tracingcore.dao.repository.*
import de.uniba.akshaychoudhry.tracingcore.tracingmodules.RiskCalculationTaskScheduler
import de.uniba.akshaychoudhry.tracingcore.utilities.*
import lombok.extern.slf4j.Slf4j
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import kotlin.collections.HashMap
import org.springframework.core.task.TaskExecutor
import org.springframework.web.bind.annotation.PathVariable
import java.util.*
import java.util.concurrent.CompletableFuture

@RestController
@RequestMapping("/tracingService")
@Slf4j
class TracingService {

	//region injected dependencies
	@Autowired
	lateinit var venueRepo: VenueRepository

	@Autowired
	lateinit var wearableLocationRepository: WearableLocationRepository

	@Autowired
	lateinit var wearableRepository: WearableRepository

	@Autowired
	lateinit var resultRepository: ResultRepository

	@Autowired
	lateinit var taskExecutor: TaskExecutor

	@Autowired
	lateinit var stayPointRepository: StayPointRepository

	@Autowired
	lateinit var trainingUtility: TrainingUtility

	@Value("\${tracing.save_results}")
	private var saveResults: Boolean = false
	//endregion

	//region class members

	val venueDescToVenueMap = HashMap<String,VenueEntity>()

	val idToWearableMap = HashMap<Int,WearableEntity>()

	val contagiousStayPointsMap = HashMap<String,MutableList<Pair<Date,Date>>>()

	lateinit var infectedTrajectory: Iterable<WearableLocationEntity>

	lateinit var infectedWearable: WearableEntity

	lateinit var riskCalculationTaskScheduler: RiskCalculationTaskScheduler

	val classificationThresholdMap = HashMap<Int,Double>()
	//endregion

	@GetMapping("/trainModel/{pow1}/{pow2}")
	fun trainModel(@PathVariable pow1: Double, @PathVariable pow2: Double) {
		val model = createScaleFunction(pow1, pow2)
		println("\n\nThis model is numbered $model")
		trainingUtility.trainModel(model)
	}

	@GetMapping("/traceContactsForWearable/{model}")
	fun traceContactsForWearable(@PathVariable model: Int) {
		startContactTracing(model)
	}

	@GetMapping("/traceContactsForWearable/")
	fun traceContactsForWearable() {
		startContactTracing(modelsTrained-1)
	}

	private fun startContactTracing(model: Int) {
		taskExecutor.execute {
			initialiseResources(model)
			CompletableFuture.supplyAsync {
				riskCalculationTaskScheduler
					.executeMultiThreadedContactTracing(modelToScaleFunctionMap[model]!!, aerosolRiskExtrapolator)
			}.thenApply {
				resultList -> classifyResults(resultList, model)
			}.thenApply {
				resultList ->
				resultList.sortedBy{it.wearable.wearableId}.forEach {
					val correctness = if (it.wearable.riskLabel == it.riskLabel) "" else "INCORRECT"
					println("Calculated Risk for wearable ${it.wearable.wearableId} = ${it.score} \t-> ${it.wearable.labelType}\t${correctness}")
				}
				resultList
			}.thenApply { resultList ->
				if(saveResults) {
					saveResults(resultList)
				}
				resultList
			}.thenAccept {
				resultList -> calculateMetrics(resultList)
			}

		}
		println("Background contact tracing initiated.")
	}

	private fun classifyResults(resultList: List<ResultEntity>, model: Int): List<ResultEntity> {
		resultList.forEach { resultEntity ->
			resultEntity.riskLabel = classifyRisk(resultEntity.score, model)
		}
		return resultList
	}

	private fun initialiseResources(model: Int) {
		fetchVenues()
		fetchInfectedTrajectory()
		fetchTrajectories()
		fetchStays()
		createRiskCalculationTaskScheduler()
		classificationThresholdMap[model] = trainingUtility.getThresholdForModel(model)
	}

	private fun fetchInfectedTrajectory() {
		infectedWearable = wearableRepository.findByWearableId(INFECTED_WEARABLE_ID)
		infectedTrajectory = wearableLocationRepository.findByWearableOrderByTimestampAsc(infectedWearable)
	}

	private fun fetchVenues() {
		venueRepo.findAllBy().forEach { venueDescToVenueMap[it.venueDesc] = it }
	}

	private fun fetchTrajectories() {
		wearableRepository.findByRiskLabel(RiskLabel.LOW).forEach {
			idToWearableMap[it.wearableId] = it
		}

		wearableRepository.findByRiskLabel(RiskLabel.HIGH).forEach {
			idToWearableMap[it.wearableId] = it
		}
	}

	private fun saveResults(resultList:List<ResultEntity>) {
		resultList.forEach { resultEntity ->
			resultRepository.save(resultEntity)
		}
	}

	private fun fetchStays() {
		val infectedTrajectoryLocal = wearableLocationRepository.findByWearableOrderByTimestampAsc(infectedWearable).toMutableList()

		var i = 0
		while (i<infectedTrajectoryLocal.size) {
			val currentVenue = infectedTrajectoryLocal[i].venue
			val start = infectedTrajectoryLocal[i].timestamp
			do {
				i++
			}while(i<infectedTrajectoryLocal.size && currentVenue.venueDesc == infectedTrajectoryLocal[i].venue.venueDesc)
			val end = infectedTrajectoryLocal[i-1].timestamp
			val stayPointEntity = StayPointEntity(start = start, end = end, venue = currentVenue)
			stayPointRepository.save(stayPointEntity)
			if( !contagiousStayPointsMap.containsKey(currentVenue.venueDesc) ) {
				contagiousStayPointsMap[currentVenue.venueDesc] = mutableListOf()
			}
			contagiousStayPointsMap[currentVenue.venueDesc]!!.add(Pair(start,end))
		}
		println("Stays determined.\n\n")
	}

	private fun createRiskCalculationTaskScheduler() {
		riskCalculationTaskScheduler = RiskCalculationTaskScheduler(
			idToWearableMap,
			contagiousStayPointsMap,
			infectedTrajectory,
			wearableRepository,
			wearableLocationRepository
		)
	}

	private fun calculateMetrics(resultList:List<ResultEntity>) {
		var truePositives = 0
		var trueNegatives = 0
		var falsePositives = 0
		var falseNegatives = 0
		resultList.forEach {
			if (it.wearable.riskLabel == it.riskLabel) {
				if (it.wearable.riskLabel == RiskLabel.LOW) {
					trueNegatives++
				} else {
					truePositives++
				}
			} else {
				if (it.wearable.riskLabel == RiskLabel.LOW) {
					falsePositives++
				} else {
					falseNegatives++
				}
			}
		}

		val trajectoriesClassifiedCorrectly = truePositives + trueNegatives
		val totalNumberOfTrajectoriesToClassify = truePositives + falsePositives + trueNegatives + falseNegatives

		//calculate validity of utility function/similarity metric
		println("total number of trajectories = $totalNumberOfTrajectoriesToClassify")
		println("trajectories classified correctly = $trajectoriesClassifiedCorrectly")
		val correctPercentage: Double = trajectoriesClassifiedCorrectly * 100.0 / totalNumberOfTrajectoriesToClassify
		println("Percentage of correct classifications = $correctPercentage")

		val precision: Double = truePositives.toDouble()/(truePositives.toDouble()+falsePositives.toDouble())
		val recall: Double = truePositives.toDouble()/(truePositives.toDouble()+falseNegatives.toDouble())
		val f1: Double = (2.0 * precision * recall)/(precision+recall)
		val accuracy: Double =(truePositives.toDouble()+trueNegatives.toDouble())/(truePositives.toDouble()+falsePositives.toDouble()+trueNegatives.toDouble()+falseNegatives.toDouble())
		val specificity: Double = trueNegatives.toDouble()/(falsePositives.toDouble()+trueNegatives.toDouble())

		println("\n\nMETRICS:")
		println("accuracy\t\t=\t$accuracy")
		println("precision\t\t=\t$precision")
		println("recall\t\t\t=\t$recall")
		println("f1\t\t\t\t=\t$f1")
		println("\nspecificity\t\t=\t$specificity")

		println("Background contact tracing ended.")
	}

	private fun classifyRisk(risk:Double, model: Int): RiskLabel {
		return when {
			risk >= classificationThresholdMap[model]!! -> {
				RiskLabel.HIGH
			}
			else -> {
				RiskLabel.LOW
			}
		}
	}
}