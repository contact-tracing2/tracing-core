package de.uniba.akshaychoudhry.tracingcore.dto

data class VenueEntityDto(val id:Long,
					   val venueSize:Double,
					   val venueDesc:String,
					   val venueLongDesc:String)