package de.uniba.akshaychoudhry.tracingcore.utilities

import java.util.*
import kotlin.collections.HashMap
import kotlin.math.absoluteValue
import kotlin.math.pow


const val SUBCOST_SCALE_FACTOR: Double = 1000.0
const val INFECTED_WEARABLE_ID:Int = 10000001
const val SUBCOST_ALTERATION_INTERVAL_MILLIS:Int = 10000
const val MAX_CONTAGIOUS_INTERVAL_HOURS:Double = 2.6
const val MAX_CONTAGIOUS_INTERVAL_MILLIS:Int = (MAX_CONTAGIOUS_INTERVAL_HOURS * 60.0 * 60.0 * 1000).toInt()
const val SUBCOST_CORRECTION_FACTOR:Double = 200.0
const val NUMBER_OF_THREADS:Int = 10
typealias ScaleFunction = (Double, Double, Double) -> Double
typealias AerosolSubcostExtrapolator = (it: Pair<Date, Date>, timestamp: Date) -> Double

val modelToScaleFunctionMap = HashMap<Int,ScaleFunction>()
var modelsTrained=0
fun createScaleFunction(pow1: Double, pow2: Double): Int {
    modelToScaleFunctionMap[modelsTrained] = { subcost:Double, venueArea:Double, venueHeight:Double -> subcost.pow(pow1)/((venueArea * venueHeight).pow(pow2))}
    return modelsTrained++
}

val aerosolRiskExtrapolator  = { it: Pair<Date, Date>, timestamp: Date ->
    val maxRiskSubcost = (it.second.subtract(it.first).toDouble())
    val slope = -maxRiskSubcost / MAX_CONTAGIOUS_INTERVAL_MILLIS
    val offsetFromEnd = timestamp.subtract(it.second).toDouble()
    ((slope * slope.absoluteValue * offsetFromEnd) + maxRiskSubcost).coerceAtLeast(0.0)
}

val ellipsoidalAerosolRiskExtrapolator = { it: Pair<Date, Date>, timestamp: Date ->
    val maxRiskSubcost = it.second.subtract(it.first).toDouble()            //(t2-t1) = h^.5
    val offsetFromEnd = timestamp.subtract(it.second).toDouble()            // x -> lies between 0 and TS i.e. T^.5
    val h = maxRiskSubcost.pow(2)
    val T = MAX_CONTAGIOUS_INTERVAL_MILLIS.toDouble().pow(2.0)
    (maxRiskSubcost-((1-(((offsetFromEnd-MAX_CONTAGIOUS_INTERVAL_MILLIS).pow(2))/T))*h).pow(.5)).coerceAtLeast(0.0)
}