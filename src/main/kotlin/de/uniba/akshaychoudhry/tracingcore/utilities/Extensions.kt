package de.uniba.akshaychoudhry.tracingcore.utilities

import java.lang.Math.pow
import java.util.*
import java.util.Calendar
import kotlin.math.pow

fun Date.nextTimestamp(sec:Int = 10):Date {
	val c = Calendar.getInstance()
	c.time = this
	c.add(Calendar.SECOND, sec)
	return c.time
}

fun Double.floor():Double {
	return kotlin.math.floor(this)
}

fun Double.log(base: Double = 10000.0):Double {
	return kotlin.math.log(this, base)
}