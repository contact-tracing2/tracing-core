package de.uniba.akshaychoudhry.tracingcore.utilities

import org.springframework.context.annotation.Bean
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor

import org.springframework.core.task.TaskExecutor


@Bean
fun getTaskExecutor(): TaskExecutor? {
	val threadPoolTaskExecutor = ThreadPoolTaskExecutor()
	threadPoolTaskExecutor.corePoolSize = 1
	threadPoolTaskExecutor.maxPoolSize = 5
	return threadPoolTaskExecutor
}