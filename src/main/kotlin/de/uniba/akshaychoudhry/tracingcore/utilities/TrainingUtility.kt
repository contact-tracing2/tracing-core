package de.uniba.akshaychoudhry.tracingcore.utilities

import de.uniba.akshaychoudhry.tracingcore.dao.entity.*
import de.uniba.akshaychoudhry.tracingcore.dao.repository.*
import de.uniba.akshaychoudhry.tracingcore.tracingmodules.RiskCalculationTaskScheduler
import lombok.extern.slf4j.Slf4j
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Scope
import kotlin.collections.HashMap
import org.springframework.core.task.TaskExecutor
import org.springframework.stereotype.Component
import java.util.*
import java.util.concurrent.CompletableFuture

@Component
@Scope("singleton")
@Slf4j
class TrainingUtility {

    //region injected dependencies
    @Autowired
    lateinit var venueRepo: VenueRepository

    @Autowired
    lateinit var wearableLocationRepository: WearableLocationRepository

    @Autowired
    lateinit var wearableRepository: WearableRepository

    @Autowired
    lateinit var taskExecutor: TaskExecutor

    @Autowired
    lateinit var stayPointRepository: StayPointRepository
    //endregion

    //region class members

    private val venueDescToVenueMap = HashMap<String,VenueEntity>()

    private val idToWearableMap = HashMap<Int,WearableEntity>()

    private val contagiousStayPointsMap = HashMap<String,MutableList<Pair<Date,Date>>>()

    lateinit var infectedTrajectory: Iterable<WearableLocationEntity>

    lateinit var infectedWearable: WearableEntity

    lateinit var riskCalculationTaskScheduler: RiskCalculationTaskScheduler

    private val thresholdMap = HashMap<Int,Double>()
    //endregion

    fun trainModel(model: Int) {
        startTrainingModel(model)
    }

    private fun startTrainingModel(model: Int) {
        taskExecutor.execute {
            initialiseTrainingResources()
            CompletableFuture.supplyAsync {
                riskCalculationTaskScheduler.executeMultiThreadedContactTracing(modelToScaleFunctionMap[model]!!,
                    aerosolRiskExtrapolator)
            }.thenApply {
                resultList ->
                resultList.forEach{
                    when (it.wearable.riskLabel) {
                        RiskLabel.TRAINING_HIGH_BSR -> {
                            it.score *= 58
                        }
                        RiskLabel.TRAINING_HIGH_T -> {
                            it.score *= 69
                        }
                        RiskLabel.TRAINING_HIGH_SSR -> {
                            it.score *= 3
                        }
                        RiskLabel.TRAINING_HIGH_BSR_N -> {
                            it.score *= 51
                        }
                        RiskLabel.TRAINING_HIGH_T_N -> {
                            it.score *= 57
                        }
                        RiskLabel.TRAINING_HIGH_SSR_N -> {
                            it.score *= 3
                        }
                    }
                    println("Score[${it.wearable.wearableId}]=${it.score}")
                }
                resultList
            }.thenApply {
                resultList ->
                calculateRecommendedThreshold(resultList)
            }.thenApply {
                thresholdMap[model] = it
            }
        }
    }

    private fun initialiseTrainingResources() {
        fetchVenues()
        fetchInfectedTrajectory()
        fetchTrajectories()
        fetchStays()
        createRiskCalculationTaskScheduler()
    }

    private fun fetchInfectedTrajectory() {
        infectedWearable = wearableRepository.findByWearableId(INFECTED_WEARABLE_ID)
        infectedTrajectory = wearableLocationRepository.findByWearableOrderByTimestampAsc(infectedWearable)
    }

    private fun fetchVenues() {
        venueRepo.findAllBy().forEach { venueDescToVenueMap[it.venueDesc] = it }
    }

    private fun fetchTrajectories() {
        wearableRepository.findByRiskLabel(RiskLabel.TRAINING_HIGH_BSR).forEach {
            idToWearableMap[it.wearableId] = it
        }

        wearableRepository.findByRiskLabel(RiskLabel.TRAINING_HIGH_SSR).forEach {
            idToWearableMap[it.wearableId] = it
        }

        wearableRepository.findByRiskLabel(RiskLabel.TRAINING_HIGH_T).forEach {
            idToWearableMap[it.wearableId] = it
        }

        wearableRepository.findByRiskLabel(RiskLabel.TRAINING_HIGH_BSR_N).forEach {
            idToWearableMap[it.wearableId] = it
        }

        wearableRepository.findByRiskLabel(RiskLabel.TRAINING_HIGH_SSR_N).forEach {
            idToWearableMap[it.wearableId] = it
        }

        wearableRepository.findByRiskLabel(RiskLabel.TRAINING_HIGH_T_N).forEach {
            idToWearableMap[it.wearableId] = it
        }
    }

    private fun fetchStays() {
        val infectedTrajectoryLocal = wearableLocationRepository.findByWearableOrderByTimestampAsc(infectedWearable).toMutableList()

        var i = 0
        while (i<infectedTrajectoryLocal.size) {
            val currentVenue = infectedTrajectoryLocal[i].venue
            val start = infectedTrajectoryLocal[i].timestamp
            do {
                i++
            }while(i<infectedTrajectoryLocal.size && currentVenue.venueDesc == infectedTrajectoryLocal[i].venue.venueDesc)
            val end = infectedTrajectoryLocal[i-1].timestamp
            val stayPointEntity = StayPointEntity(start = start, end = end, venue = currentVenue)
//            println("Venue=${currentVenue.venueDesc}\tstart=$start\tend=$end")
            stayPointRepository.save(stayPointEntity)
            if( !contagiousStayPointsMap.containsKey(currentVenue.venueDesc) ) {
                contagiousStayPointsMap[currentVenue.venueDesc] = mutableListOf()
            }
            contagiousStayPointsMap[currentVenue.venueDesc]!!.add(Pair(start,end))
        }
        println("Stays determined.\n\n")
    }

    private fun createRiskCalculationTaskScheduler() {
        riskCalculationTaskScheduler = RiskCalculationTaskScheduler(
            idToWearableMap,
            contagiousStayPointsMap,
            infectedTrajectory,
            wearableRepository,
            wearableLocationRepository
        )
    }

    private fun calculateRecommendedThreshold(resultList:List<ResultEntity>):Double {
        var bestThreshold = Double.MAX_VALUE
        resultList.forEach {
            var newThreshold = it.score
            if (bestThreshold > newThreshold) {
                bestThreshold = newThreshold
            }
        }
        println("Training Completed.\nBestThreshold=${bestThreshold}")
        return bestThreshold
    }

    fun getThresholdForModel(model:Int):Double {
        return thresholdMap[model]!!
    }

}