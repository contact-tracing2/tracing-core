package de.uniba.akshaychoudhry.tracingcore.utilities

import java.util.*
import java.util.concurrent.CompletableFuture
import java.util.stream.Collectors

fun Date.subtract(otherDate: Date): Long {
	return (time - otherDate.time)
}

fun Double.getSubcost():Double {
	return this + 1.0
}

fun Double.getSubcost(roomSize:Double):Double {
	//venue area consideration
	return this + SUBCOST_SCALE_FACTOR/roomSize
}

fun <T> all(futures: List<CompletableFuture<T>>): CompletableFuture<List<T>> {
	val cfs = futures.toTypedArray<CompletableFuture<*>>()
	return CompletableFuture.allOf(*cfs)
		.thenApply { _: Void? ->
			futures.stream()
				.map { obj: CompletableFuture<T> -> obj.join() }
				.collect(Collectors.toList())
		}
}
