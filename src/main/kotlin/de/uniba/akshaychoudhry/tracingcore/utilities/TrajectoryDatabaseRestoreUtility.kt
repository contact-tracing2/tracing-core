package de.uniba.akshaychoudhry.tracingcore.utilities

import de.uniba.akshaychoudhry.tracingcore.dao.entity.*
import de.uniba.akshaychoudhry.tracingcore.dao.repository.VenueRepository
import de.uniba.akshaychoudhry.tracingcore.dao.repository.WearableLocationRepository
import de.uniba.akshaychoudhry.tracingcore.dao.repository.WearableRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap
import org.springframework.boot.ApplicationArguments

import org.springframework.boot.ApplicationRunner
import org.springframework.core.task.TaskExecutor
import java.util.concurrent.Executors


@Component
class TrajectoryDatabaseRestoreUtility: ApplicationRunner  {

	@Autowired
	lateinit var venueRepo: VenueRepository

	@Autowired
	lateinit var wearableLocationRepository: WearableLocationRepository

	@Autowired
	lateinit var wearableRepository: WearableRepository

	@Autowired
	lateinit var taskExecutor: TaskExecutor

	@Value("\${spring.jpa.hibernate.ddl-auto}")
	private var ddlAuto: String = ""

	val venueDescToVenueMap = HashMap<String,VenueEntity>()

	//region date array
	private final val format = "yyyy-MM-dd HH:mm:ss"
	val formatter = SimpleDateFormat(format, Locale.GERMANY)
	val dates = Array(5) { i ->
		val day = i + 4
		Array<Date>(5) { j ->
			var hour = (8 + j * 2).toString()
			hour = if (hour.length < 10) "0$hour" else hour
			formatter.parse("2021-01-0$day $hour:15:00")
		}
	}
	//endregion

	override fun run(args: ApplicationArguments?) {
		//TODO find a way to sync DB instead of restoring all this
		if(ddlAuto.contains("create")) {
			createDatabaseEntries()
		}
	}

	private fun createDatabaseEntries() {
		createVenues()
		createInfectedTrajectory()

		createTrainingHighTrajectory1()
		createTrainingHighTrajectory2()
		createTrainingHighTrajectory3()
		createTrainingHighTrajectory4()
		createTrainingHighTrajectory5()
		createTrainingHighTrajectory6()

//		createLowTrajectory1()
//		createLowTrajectory2()
//		createLowTrajectory3()
		createLowTrajectory4()
		createLowTrajectory5()
		createLowTrajectory6()
		createLowTrajectory7()
		createLowTrajectory8()
		createLowTrajectory9()
		createLowTrajectory10()
		createLowTrajectory11()
		createLowTrajectory12()
		createLowTrajectory13()
		createLowTrajectory14()
		createLowTrajectory15()
		createLowTrajectory16()
		createLowTrajectory17()
		createLowTrajectory18()

		createHighTrajectory1()
		createHighTrajectory2()
		createHighTrajectory3()
		createHighTrajectory4()
		createHighTrajectory5()
		createHighTrajectory6()
		createHighTrajectory7()
		createHighTrajectory8()
		createHighTrajectory9()
		createHighTrajectory10()
		createHighTrajectory11()
		createHighTrajectory12()
		createHighTrajectory13()
		createHighTrajectory14()
		createHighTrajectory15()
	}

	private fun createVenues() {

		val v1 = VenueEntity(venueSize=240.0, venueDesc = "BSR1", venueLongDesc = "Big Seminar Room", venueHeight = 5.0, wearableLocations = null, stayPoints =  null)
		val v2 = VenueEntity(venueSize=240.0, venueDesc = "BSR2", venueLongDesc = "Big Seminar Room", venueHeight = 5.0, wearableLocations = null, stayPoints =  null)
		val v3 = VenueEntity(venueSize=240.0, venueDesc = "BSR3", venueLongDesc = "Big Seminar Room", venueHeight = 5.0, wearableLocations = null, stayPoints =  null)
		val v4 = VenueEntity(venueSize=240.0, venueDesc = "BSR4", venueLongDesc = "Big Seminar Room", venueHeight = 5.0, wearableLocations = null, stayPoints =  null)
		val v5 = VenueEntity(venueSize=240.0, venueDesc = "BSR5", venueLongDesc = "Big Seminar Room", venueHeight = 5.0, wearableLocations = null, stayPoints =  null)

		val v6 = VenueEntity(venueSize=50.0, venueDesc = "SSR1", venueLongDesc = "Small Seminar Room", wearableLocations = null, stayPoints =  null)
		val v7 = VenueEntity(venueSize=50.0, venueDesc = "SSR2", venueLongDesc = "Small Seminar Room", wearableLocations = null, stayPoints =  null)
		val v8 = VenueEntity(venueSize=50.0, venueDesc = "SSR3", venueLongDesc = "Small Seminar Room", wearableLocations = null, stayPoints =  null)
		val v9 = VenueEntity(venueSize=50.0, venueDesc = "SSR4", venueLongDesc = "Small Seminar Room", wearableLocations = null, stayPoints =  null)
		val v10 = VenueEntity(venueSize=50.0, venueDesc = "SSR5", venueLongDesc = "Small Seminar Room", wearableLocations = null, stayPoints =  null)

		val v11 = VenueEntity(venueSize=15.0, venueDesc = "T1", venueLongDesc = "Toilet", wearableLocations = null, stayPoints =  null)

		venueRepo.save(v1)
		venueRepo.save(v2)
		venueRepo.save(v3)
		venueRepo.save(v4)
		venueRepo.save(v5)
		venueRepo.save(v6)
		venueRepo.save(v7)
		venueRepo.save(v8)
		venueRepo.save(v9)
		venueRepo.save(v10)
		venueRepo.save(v11)

		venueRepo.findAllBy().forEach { venueDescToVenueMap[it.venueDesc] = it }

	}

	fun addPointsForVenueLocation(wearable: WearableEntity, date: Date, venueDesc: String, numberOfPoints: Int = 360) {
		taskExecutor.execute {
			var curDate = date
			val venue = venueDescToVenueMap[venueDesc]!!

			for (i in 1..numberOfPoints) {
				val point = WearableLocationEntity(timestamp = curDate, venue = venue, wearable = wearable)
				wearableLocationRepository.save(point)
				curDate = curDate.nextTimestamp()
			}
			println("Chunk of trajectory with ID ${wearable.wearableId} for $date created.")
		}
	}

	private fun createInfectedTrajectory() {
		/* infected trajectory 1 Mon -> Fri
		 *
		 * M: 8 BSR1 10 T1 12 SSR1
		 * T: 8 BSR1 10 T1 12 SSR1 14 T1 16 SSR2
		 * W: 8 BSR2 10 T1
		 * T: 8 BSR2 10 T1 12 SSR3
		 * F: 8 SSR3 10 T1 12 SSR2
		 *
		 */

		//region data chunks
		val wearable = WearableEntity(wearableId = INFECTED_WEARABLE_ID, riskLabel = RiskLabel.POSITIVE)
		wearableRepository.save(wearable)

		addPointsForVenueLocation(wearable, date = dates[0][0], venueDesc = "BSR1")
		addPointsForVenueLocation(wearable, date = dates[0][1], venueDesc = "T1", numberOfPoints = 90)
		addPointsForVenueLocation(wearable, date = dates[0][2], venueDesc = "SSR1")

		addPointsForVenueLocation(wearable, date = dates[1][0], venueDesc = "BSR1")
		addPointsForVenueLocation(wearable, date = dates[1][1], venueDesc = "T1",  numberOfPoints = 90)
		addPointsForVenueLocation(wearable, date = dates[1][2], venueDesc = "SSR1")
		addPointsForVenueLocation(wearable, date = dates[1][3], venueDesc = "T1",  numberOfPoints = 90)
		addPointsForVenueLocation(wearable, date = dates[1][4], venueDesc = "SSR2")

		addPointsForVenueLocation(wearable, date = dates[2][0], venueDesc = "BSR2")
		addPointsForVenueLocation(wearable, date = dates[2][1], venueDesc = "T1",  numberOfPoints = 90)

		addPointsForVenueLocation(wearable, date = dates[3][0], venueDesc = "BSR2")
		addPointsForVenueLocation(wearable, date = dates[3][1], venueDesc = "T1",  numberOfPoints = 90)
		addPointsForVenueLocation(wearable, date = dates[3][2], venueDesc = "SSR3")

		addPointsForVenueLocation(wearable, date = dates[4][0], venueDesc = "SSR3")
		addPointsForVenueLocation(wearable, date = dates[4][1], venueDesc = "T1",  numberOfPoints = 90)
		addPointsForVenueLocation(wearable, date = dates[4][2], venueDesc = "SSR2")
		//endregion
	}

	//region low data insertion
	private fun createLowTrajectory1() {
		/* low trajectory 1 Mon -> Fri
		 *
		 * M: 8 BSR1
		 *
		 */

		val wearable = WearableEntity(wearableId = 20000001, riskLabel = RiskLabel.LOW)
		wearableRepository.save(wearable)

		addPointsForVenueLocation(wearable, date = dates[0][0], venueDesc = "BSR1")
	}

	private fun createLowTrajectory2() {
		/* low trajectory 1 Mon -> Fri
		 *
		 * M: 10 BSR1
		 *
		 */

		val wearable = WearableEntity(wearableId = 20000002, riskLabel = RiskLabel.LOW)
		wearableRepository.save(wearable)

		addPointsForVenueLocation(wearable, date = dates[0][1], venueDesc = "BSR1")
	}

	private fun createLowTrajectory3() {
		/* infected trajectory 2 Mon -> Fri
		 *
		 * M: 1015 T1
		 *
		 */

		val wearable = WearableEntity(wearableId = 20000003, riskLabel = RiskLabel.LOW)
		wearableRepository.save(wearable)

		addPointsForVenueLocation(wearable, date = dates[0][2].nextTimestamp(900), venueDesc = "T1", numberOfPoints = 90)
	}

	private fun createLowTrajectory4() {
		/* infected highest 1 Mon -> Fri
		 *
		 * M: 8 BSR1
		 * T: 8 BSR1
		 * W: 8 BSR2
		 *
		 */

		val wearable = WearableEntity(wearableId = 20000004, riskLabel = RiskLabel.LOW)
		wearableRepository.save(wearable)

		addPointsForVenueLocation(wearable, date = dates[0][0], venueDesc = "BSR1")

		addPointsForVenueLocation(wearable, date = dates[1][0], venueDesc = "BSR1")

		addPointsForVenueLocation(wearable, date = dates[2][0], venueDesc = "BSR2")
	}

	private fun createLowTrajectory5() {
		/* infected trajectory 1 Mon -> Fri
		 *
		 * M: 10 BSR1
		 * T: 10 BSR1
		 * W: 10 BSR2
		 * T: 10 BSR2
		 * F: 10 SSR3
		 *
		 */

		val wearable = WearableEntity(wearableId = 20000005, riskLabel = RiskLabel.LOW)
		wearableRepository.save(wearable)

		addPointsForVenueLocation(wearable, date = dates[0][1], venueDesc = "BSR1")

		addPointsForVenueLocation(wearable, date = dates[1][1], venueDesc = "BSR1")

		addPointsForVenueLocation(wearable, date = dates[2][1], venueDesc = "BSR2")

		addPointsForVenueLocation(wearable, date = dates[3][1], venueDesc = "BSR2")

		addPointsForVenueLocation(wearable, date = dates[4][1], venueDesc = "SSR3")
	}

	private fun createLowTrajectory6() {
		/* infected trajectory 1 Mon -> Fri
		 *
		 * M: 14 SSR1
		 * T: 14 SSR1
		 *
		 */

		val wearable = WearableEntity(wearableId = 20000006, riskLabel = RiskLabel.LOW, labelType = LabelType.EDGE)
		wearableRepository.save(wearable)

		addPointsForVenueLocation(wearable, date = dates[0][3], venueDesc = "SSR1")
		addPointsForVenueLocation(wearable, date = dates[1][3], venueDesc = "SSR1")
	}

	private fun createLowTrajectory7() {
		/* infected trajectory 1 Mon -> Fri
		 *
		 * M: 12 T1
		 *
		 */

		val wearable = WearableEntity(wearableId = 20000007, riskLabel = RiskLabel.LOW)
		wearableRepository.save(wearable)

		addPointsForVenueLocation(wearable, date = dates[0][2], venueDesc = "T1", numberOfPoints = 90)
	}

	private fun createLowTrajectory8() {
		/* infected trajectory 1 Mon -> Fri
		 *
		 * M: 8 BSR1 12 SSR1
		 *
		 */

		val wearable = WearableEntity(wearableId = 20000008, riskLabel = RiskLabel.LOW)
		wearableRepository.save(wearable)

		addPointsForVenueLocation(wearable, date = dates[0][0], venueDesc = "BSR1")
		addPointsForVenueLocation(wearable, date = dates[0][2], venueDesc = "SSR1")
	}

	private fun createLowTrajectory9() {
		/* low trajectory 9 Mon -> Fri
		 *
		 * M: 10 T1
		 *
		 */

		val wearable = WearableEntity(wearableId = 20000009, riskLabel = RiskLabel.LOW)
		wearableRepository.save(wearable)

		addPointsForVenueLocation(wearable, date = dates[0][1], venueDesc = "T1", numberOfPoints = 90)
	}

	private fun createLowTrajectory10() {
		/* low trajectory 10 Mon -> Fri
		 *
		 * M: 1010 T1
		 *
		 */

		val wearable = WearableEntity(wearableId = 20000010, riskLabel = RiskLabel.LOW)
		wearableRepository.save(wearable)

		addPointsForVenueLocation(wearable, date = dates[0][1].nextTimestamp(600), venueDesc = "T1", numberOfPoints = 90)
	}

	private fun createLowTrajectory11() {
		/* infected highest 1 Mon -> Fri
		 *
		 * M: 8 BSR1
		 * T: 8 BSR1
		 * W: 8 BSR2
		 * T: 8 BSR2
		 *
		 */

		val wearable = WearableEntity(wearableId = 20000011, riskLabel = RiskLabel.LOW)
		wearableRepository.save(wearable)

		addPointsForVenueLocation(wearable, date = dates[0][0], venueDesc = "BSR1")

		addPointsForVenueLocation(wearable, date = dates[1][0], venueDesc = "BSR1")

		addPointsForVenueLocation(wearable, date = dates[2][0], venueDesc = "BSR2")

		addPointsForVenueLocation(wearable, date = dates[3][0], venueDesc = "BSR2")
	}

	private fun createLowTrajectory12() {
		/* infected trajectory 1 Mon -> Fri
		 *
		 * M: 14 SSR1
		 * T: 14 SSR1
		 *
		 */

		val wearable = WearableEntity(wearableId = 20000012, riskLabel = RiskLabel.LOW)
		wearableRepository.save(wearable)

		addPointsForVenueLocation(wearable, date = dates[0][3], venueDesc = "SSR1")

		addPointsForVenueLocation(wearable, date = dates[1][3], venueDesc = "SSR1")
	}

	private fun createLowTrajectory13() {
		/* infected trajectory 1 Mon -> Fri
		 *
		 * M: 12 T1
		 * T: 12 T1
		 *
		 */

		val wearable = WearableEntity(wearableId = 20000013, riskLabel = RiskLabel.LOW)
		wearableRepository.save(wearable)

		addPointsForVenueLocation(wearable, date = dates[0][2], venueDesc = "T1", numberOfPoints = 90)
		addPointsForVenueLocation(wearable, date = dates[1][2], venueDesc = "T1",  numberOfPoints = 90)
	}

	private fun createLowTrajectory14() {
		/* infected trajectory 1 Mon -> Fri
		 *
		 * M: 8 BSR1 10 T1
		 * T: 8 BSR1 10 T1 14 T1
		 * W: 8 BSR2 10 T1
		 * T: 8 BSR2 10 T1
		 * F: 10 T1
		 *
		 */

		//region data chunks
		val wearable = WearableEntity(wearableId = 20000014, riskLabel = RiskLabel.LOW)
		wearableRepository.save(wearable)

		addPointsForVenueLocation(wearable, date = dates[0][0], venueDesc = "BSR1")
		addPointsForVenueLocation(wearable, date = dates[0][1], venueDesc = "T1", numberOfPoints = 90)

		addPointsForVenueLocation(wearable, date = dates[1][0], venueDesc = "BSR1")
		addPointsForVenueLocation(wearable, date = dates[1][1], venueDesc = "T1",  numberOfPoints = 90)
		addPointsForVenueLocation(wearable, date = dates[1][3], venueDesc = "T1",  numberOfPoints = 90)

		addPointsForVenueLocation(wearable, date = dates[2][0], venueDesc = "BSR2")
		addPointsForVenueLocation(wearable, date = dates[2][1], venueDesc = "T1",  numberOfPoints = 90)

		addPointsForVenueLocation(wearable, date = dates[3][0], venueDesc = "BSR2")
		addPointsForVenueLocation(wearable, date = dates[3][1], venueDesc = "T1",  numberOfPoints = 90)

		addPointsForVenueLocation(wearable, date = dates[4][1], venueDesc = "T1",  numberOfPoints = 90)
		//endregion
	}

	private fun createLowTrajectory15() {
		/* infected trajectory 1 Mon -> Fri
		 *
		 * M: 8 BSR1 10 T1 12 SSR1
		 * T: 8 BSR1 10 T1 12 SSR1 14 T1
		 * W: 8 BSR2 10 T1
		 * T: 8 BSR2 10 T1
		 * F:
		 *
		 */

		val wearable = WearableEntity(wearableId = 20000015, riskLabel = RiskLabel.LOW, labelType = LabelType.EDGE)
		wearableRepository.save(wearable)

		addPointsForVenueLocation(wearable, date = dates[0][0], venueDesc = "BSR1")
		addPointsForVenueLocation(wearable, date = dates[0][1], venueDesc = "T1", numberOfPoints = 90)
		addPointsForVenueLocation(wearable, date = dates[0][2], venueDesc = "SSR1")

		addPointsForVenueLocation(wearable, date = dates[1][0], venueDesc = "BSR1")
		addPointsForVenueLocation(wearable, date = dates[1][1], venueDesc = "T1",  numberOfPoints = 90)
		addPointsForVenueLocation(wearable, date = dates[1][2], venueDesc = "SSR1")
		addPointsForVenueLocation(wearable, date = dates[1][3], venueDesc = "T1",  numberOfPoints = 90)

		addPointsForVenueLocation(wearable, date = dates[2][0], venueDesc = "BSR2")
		addPointsForVenueLocation(wearable, date = dates[2][1], venueDesc = "T1",  numberOfPoints = 90)

		addPointsForVenueLocation(wearable, date = dates[3][0], venueDesc = "BSR2")
		addPointsForVenueLocation(wearable, date = dates[3][1], venueDesc = "T1",  numberOfPoints = 90)

		addPointsForVenueLocation(wearable, date = dates[4][0], venueDesc = "SSR3")
		addPointsForVenueLocation(wearable, date = dates[4][1], venueDesc = "T1",  numberOfPoints = 90)
	}

	private fun createLowTrajectory16() {
		/* infected trajectory 1 Mon -> Fri
		 *
		 * M: 12 SSR1
		 * T: 12 SSR1
		 * W:
		 * T:
		 * F:
		 *
		 */

		val wearable = WearableEntity(wearableId = 20000016, riskLabel = RiskLabel.LOW, labelType = LabelType.BULK)
		wearableRepository.save(wearable)

		addPointsForVenueLocation(wearable, date = dates[0][2], venueDesc = "SSR1")

		addPointsForVenueLocation(wearable, date = dates[1][2], venueDesc = "SSR1")
	}

	private fun createLowTrajectory17() {
		/* infected trajectory 1 Mon -> Fri
		 *
		 * M: 10 BSR1 1015 T1 12 SSR1
		 * T: 10 BSR1 1015 T1 12 SSR1 1415 T1
		 * W: 10 BSR2 1015 T1
		 * T: 10 BSR2 1015 T1
		 * F: 1015 T1
		 *
		 */

		val wearable = WearableEntity(wearableId = 20000017, riskLabel = RiskLabel.LOW, labelType = LabelType.EDGE)
		wearableRepository.save(wearable)

		addPointsForVenueLocation(wearable, date = dates[0][0], venueDesc = "BSR1")
		addPointsForVenueLocation(wearable, date = dates[0][1].nextTimestamp(900), venueDesc = "T1", numberOfPoints = 90)
		addPointsForVenueLocation(wearable, date = dates[0][2], venueDesc = "SSR1")

		addPointsForVenueLocation(wearable, date = dates[1][0], venueDesc = "BSR1")
		addPointsForVenueLocation(wearable, date = dates[1][1].nextTimestamp(900), venueDesc = "T1",  numberOfPoints = 90)
		addPointsForVenueLocation(wearable, date = dates[1][2], venueDesc = "SSR1")
		addPointsForVenueLocation(wearable, date = dates[1][3].nextTimestamp(900), venueDesc = "T1",  numberOfPoints = 90)

		addPointsForVenueLocation(wearable, date = dates[2][0], venueDesc = "BSR2")
		addPointsForVenueLocation(wearable, date = dates[2][1].nextTimestamp(900), venueDesc = "T1",  numberOfPoints = 90)

		addPointsForVenueLocation(wearable, date = dates[3][0], venueDesc = "BSR2")
		addPointsForVenueLocation(wearable, date = dates[3][1].nextTimestamp(900), venueDesc = "T1",  numberOfPoints = 90)

		addPointsForVenueLocation(wearable, date = dates[4][1].nextTimestamp(900), venueDesc = "T1",  numberOfPoints = 90)
	}

	private fun createLowTrajectory18() {
		/* infected trajectory 1 Mon -> Fri
		 *
		 * M: 10 BSR1 1015 T1 12 SSR1
		 * T: 10 BSR1 1015 T1 12 SSR1 1415 T1
		 * W: 10 BSR2 1015 T1
		 * T: 10 BSR2
		 * F:
		 *
		 */

		val wearable = WearableEntity(wearableId = 20000018, riskLabel = RiskLabel.LOW, labelType = LabelType.EDGE)
		wearableRepository.save(wearable)

		addPointsForVenueLocation(wearable, date = dates[0][0], venueDesc = "BSR1")
		addPointsForVenueLocation(wearable, date = dates[0][1].nextTimestamp(900), venueDesc = "T1", numberOfPoints = 90)
		addPointsForVenueLocation(wearable, date = dates[0][2], venueDesc = "SSR1")

		addPointsForVenueLocation(wearable, date = dates[1][0], venueDesc = "BSR1")
		addPointsForVenueLocation(wearable, date = dates[1][1].nextTimestamp(900), venueDesc = "T1",  numberOfPoints = 90)
		addPointsForVenueLocation(wearable, date = dates[1][2], venueDesc = "SSR1")
		addPointsForVenueLocation(wearable, date = dates[1][3].nextTimestamp(900), venueDesc = "T1",  numberOfPoints = 90)

		addPointsForVenueLocation(wearable, date = dates[2][0], venueDesc = "BSR2")
		addPointsForVenueLocation(wearable, date = dates[2][1].nextTimestamp(900), venueDesc = "T1",  numberOfPoints = 90)

		addPointsForVenueLocation(wearable, date = dates[3][0], venueDesc = "BSR2")
	}
	//endregion



	//region high data insertion
	private fun createHighTrajectory1() {
		/* infected trajectory 1 Mon -> Fri
		 *
		 * M: 12 SSR1
		 * T: 12 SSR1 16 SSR2
		 * W:
		 * T:
		 * F:
		 *
		 */

		val wearable = WearableEntity(wearableId = 30000001, riskLabel = RiskLabel.HIGH, labelType = LabelType.EDGE)
		wearableRepository.save(wearable)

		addPointsForVenueLocation(wearable, date = dates[0][2], venueDesc = "SSR1")

		addPointsForVenueLocation(wearable, date = dates[1][2], venueDesc = "SSR1")
		addPointsForVenueLocation(wearable, date = dates[1][4], venueDesc = "SSR2")

	}

	private fun createHighTrajectory2() {
		/* infected trajectory 1 Mon -> Fri
		 *
		 * M: 14 SSR1
		 * T: 14 SSR1 18 SSR2
		 * W: -
		 * T: 14 SSR3
		 *
		 */

		val wearable = WearableEntity(wearableId = 30000002, riskLabel = RiskLabel.HIGH)
		wearableRepository.save(wearable)

		addPointsForVenueLocation(wearable, date = dates[0][3], venueDesc = "SSR1")
		addPointsForVenueLocation(wearable, date = dates[1][3], venueDesc = "SSR1")
		addPointsForVenueLocation(wearable, date = dates[1][4], venueDesc = "SSR2")

		addPointsForVenueLocation(wearable, date = dates[3][3], venueDesc = "SSR3")
	}

	private fun createHighTrajectory3() {
		/* infected highest 1 Mon -> Fri
		 *
		 * M: 8 BSR1 10 T1 12 SSR1
		 * T: 8 BSR1 10 T1 12 SSR1 14 T1 16 SSR2
		 * W: 8 BSR2 10 T1
		 * T: 8 BSR2 10 T1 12 SSR3
		 * F: 8 SSR3 10 T1 12 SSR2
		 *
		 */

		val wearable = WearableEntity(wearableId = 30000003, riskLabel = RiskLabel.HIGH)
		wearableRepository.save(wearable)

		addPointsForVenueLocation(wearable, date = dates[0][0], venueDesc = "BSR1")
		addPointsForVenueLocation(wearable, date = dates[0][1], venueDesc = "T1", numberOfPoints = 90)
		addPointsForVenueLocation(wearable, date = dates[0][2], venueDesc = "SSR1")

		addPointsForVenueLocation(wearable, date = dates[1][0], venueDesc = "BSR1")
		addPointsForVenueLocation(wearable, date = dates[1][1], venueDesc = "T1",  numberOfPoints = 90)
		addPointsForVenueLocation(wearable, date = dates[1][2], venueDesc = "SSR1")
		addPointsForVenueLocation(wearable, date = dates[1][3], venueDesc = "T1",  numberOfPoints = 90)
		addPointsForVenueLocation(wearable, date = dates[1][4], venueDesc = "SSR2")

		addPointsForVenueLocation(wearable, date = dates[2][0], venueDesc = "BSR2")
		addPointsForVenueLocation(wearable, date = dates[2][1], venueDesc = "T1",  numberOfPoints = 90)

		addPointsForVenueLocation(wearable, date = dates[3][0], venueDesc = "BSR2")
		addPointsForVenueLocation(wearable, date = dates[3][1], venueDesc = "T1",  numberOfPoints = 90)
		addPointsForVenueLocation(wearable, date = dates[3][2], venueDesc = "SSR3")

		addPointsForVenueLocation(wearable, date = dates[4][0], venueDesc = "SSR3")
		addPointsForVenueLocation(wearable, date = dates[4][1], venueDesc = "T1",  numberOfPoints = 90)
		addPointsForVenueLocation(wearable, date = dates[4][2], venueDesc = "SSR2")
	}

	private fun createHighTrajectory4() {
		/* infected trajectory 1 Mon -> Fri
		 *
		 * M: 8 BSR1 12 SSR1
		 * T: 8 BSR1 12 SSR1 16 SSR2
		 * W: 8 BSR2
		 * T: 8 BSR2 12 SSR3
		 * F: 8 SSR3 12 SSR2
		 *
		 */

		val wearable = WearableEntity(wearableId = 30000004, riskLabel = RiskLabel.HIGH)
		wearableRepository.save(wearable)

		addPointsForVenueLocation(wearable, date = dates[0][0], venueDesc = "BSR1")
		addPointsForVenueLocation(wearable, date = dates[0][2], venueDesc = "SSR1")

		addPointsForVenueLocation(wearable, date = dates[1][0], venueDesc = "BSR1")
		addPointsForVenueLocation(wearable, date = dates[1][2], venueDesc = "SSR1")
		addPointsForVenueLocation(wearable, date = dates[1][4], venueDesc = "SSR2")

		addPointsForVenueLocation(wearable, date = dates[2][0], venueDesc = "BSR2")

		addPointsForVenueLocation(wearable, date = dates[3][0], venueDesc = "BSR2")
		addPointsForVenueLocation(wearable, date = dates[3][2], venueDesc = "SSR3")

		addPointsForVenueLocation(wearable, date = dates[4][0], venueDesc = "SSR3")
		addPointsForVenueLocation(wearable, date = dates[4][2], venueDesc = "SSR2")
	}

	private fun createHighTrajectory5() {
		/* infected trajectory 1 Mon -> Fri
		 *
		 * M: 10 T1 12 SSR1
		 * T: 10 T1 12 SSR1 14 T1 16 SSR2
		 * W: 10 T1
		 * T: 10 T1 12 SSR3
		 * F: 8 SSR3 10 T1 12 SSR2
		 *
		 */

		val wearable = WearableEntity(wearableId = 30000005, riskLabel = RiskLabel.HIGH)
		wearableRepository.save(wearable)

		addPointsForVenueLocation(wearable, date = dates[0][1], venueDesc = "T1", numberOfPoints = 90)
		addPointsForVenueLocation(wearable, date = dates[0][2], venueDesc = "SSR1")

		addPointsForVenueLocation(wearable, date = dates[1][1], venueDesc = "T1",  numberOfPoints = 90)
		addPointsForVenueLocation(wearable, date = dates[1][2], venueDesc = "SSR1")
		addPointsForVenueLocation(wearable, date = dates[1][3], venueDesc = "T1",  numberOfPoints = 90)
		addPointsForVenueLocation(wearable, date = dates[1][4], venueDesc = "SSR2")

		addPointsForVenueLocation(wearable, date = dates[2][1], venueDesc = "T1",  numberOfPoints = 90)

		addPointsForVenueLocation(wearable, date = dates[3][1], venueDesc = "T1",  numberOfPoints = 90)
		addPointsForVenueLocation(wearable, date = dates[3][2], venueDesc = "SSR3")

		addPointsForVenueLocation(wearable, date = dates[4][0], venueDesc = "SSR3")
		addPointsForVenueLocation(wearable, date = dates[4][1], venueDesc = "T1",  numberOfPoints = 90)
		addPointsForVenueLocation(wearable, date = dates[4][2], venueDesc = "SSR2")
	}

	private fun createHighTrajectory6() {
		/* infected trajectory 1 Mon -> Fri
		 *
		 * M: 12 SSR1
		 * T: 12 SSR1 16 SSR2
		 * W:
		 * T: 12 SSR3
		 * F: 8 SSR3
		 *
		 */

		val wearable = WearableEntity(wearableId = 30000006, riskLabel = RiskLabel.HIGH)
		wearableRepository.save(wearable)

		addPointsForVenueLocation(wearable, date = dates[0][2], venueDesc = "SSR1")

		addPointsForVenueLocation(wearable, date = dates[1][2], venueDesc = "SSR1")
		addPointsForVenueLocation(wearable, date = dates[1][4], venueDesc = "SSR2")


		addPointsForVenueLocation(wearable, date = dates[3][2], venueDesc = "SSR3")

		addPointsForVenueLocation(wearable, date = dates[4][0], venueDesc = "SSR3")
	}

	private fun createHighTrajectory7() {
		/* infected trajectory 1 Mon -> Fri
		 *
		 * M: 12 SSR1
		 * T: 12 SSR1 16 SSR2
		 * W:
		 * T: 12 SSR3
		 * F:
		 *
		 */

		val wearable = WearableEntity(wearableId = 30000007, riskLabel = RiskLabel.HIGH)
		wearableRepository.save(wearable)

		addPointsForVenueLocation(wearable, date = dates[0][2], venueDesc = "SSR1")

		addPointsForVenueLocation(wearable, date = dates[1][2], venueDesc = "SSR1")
		addPointsForVenueLocation(wearable, date = dates[1][4], venueDesc = "SSR2")


		addPointsForVenueLocation(wearable, date = dates[3][2], venueDesc = "SSR3")

	}

	private fun createHighTrajectory8() {
		/* infected trajectory 1 Mon -> Fri
		 *
		 * M: 14 SSR1
		 * T: 14 SSR1
		 * W:
		 * T: 12 SSR3
		 * F:
		 *
		 */

		val wearable = WearableEntity(wearableId = 30000008, riskLabel = RiskLabel.HIGH)
		wearableRepository.save(wearable)

		addPointsForVenueLocation(wearable, date = dates[0][3], venueDesc = "SSR1")

		addPointsForVenueLocation(wearable, date = dates[1][3], venueDesc = "SSR1")

		addPointsForVenueLocation(wearable, date = dates[3][3], venueDesc = "SSR3")

	}

	private fun createHighTrajectory9() {
		/* infected trajectory 1 Mon -> Fri
		 *
		 * M: 8 BSR1 10 T1 12 SSR1
		 * T: 12 SSR1 16 SSR2
		 *
		 */

		val wearable = WearableEntity(wearableId = 30000009, riskLabel = RiskLabel.HIGH, labelType = LabelType.EDGE)
		wearableRepository.save(wearable)

		addPointsForVenueLocation(wearable, date = dates[0][0], venueDesc = "BSR1")
		addPointsForVenueLocation(wearable, date = dates[0][1], venueDesc = "T1", numberOfPoints = 90)
		addPointsForVenueLocation(wearable, date = dates[0][2], venueDesc = "SSR1")

		addPointsForVenueLocation(wearable, date = dates[1][2], venueDesc = "SSR1")
		addPointsForVenueLocation(wearable, date = dates[1][4], venueDesc = "SSR2")
	}

	private fun createHighTrajectory10() {
		/* infected trajectory 1 Mon -> Fri
		 *
		 * M: 10 BSR1 12 T1 14 SSR1
		 * T: 14 SSR1
		 * W:
		 * T: 14 SSR3
		 * F:
		 *
		 */

		val wearable = WearableEntity(wearableId = 30000010, riskLabel = RiskLabel.HIGH)
		wearableRepository.save(wearable)

		addPointsForVenueLocation(wearable, date = dates[0][1], venueDesc = "BSR1")
		addPointsForVenueLocation(wearable, date = dates[0][2], venueDesc = "T1", numberOfPoints = 90)
		addPointsForVenueLocation(wearable, date = dates[0][3], venueDesc = "SSR1")

		addPointsForVenueLocation(wearable, date = dates[1][3], venueDesc = "SSR1")

		addPointsForVenueLocation(wearable, date = dates[3][3], venueDesc = "SSR3")
	}

	private fun createHighTrajectory11() {
		/* infected trajectory 1 Mon -> Fri
		 *
		 * M: 8 BSR1 10 T1 12 SSR1
		 * T: 8 BSR1 10 T1 12 SSR1 14 T1
		 * W: 8 BSR2 10 T1
		 * T: 8 BSR2 10 T1
		 * F:
		 *
		 */

		val wearable = WearableEntity(wearableId = 30000011, riskLabel = RiskLabel.HIGH, labelType = LabelType.EDGE)
		wearableRepository.save(wearable)

		addPointsForVenueLocation(wearable, date = dates[0][1], venueDesc = "BSR1")
		addPointsForVenueLocation(wearable, date = dates[0][1].nextTimestamp(900), venueDesc = "T1", numberOfPoints = 90)
		addPointsForVenueLocation(wearable, date = dates[0][3], venueDesc = "SSR1")

		addPointsForVenueLocation(wearable, date = dates[1][1], venueDesc = "BSR1")
		addPointsForVenueLocation(wearable, date = dates[1][1].nextTimestamp(900), venueDesc = "T1",  numberOfPoints = 90)
		addPointsForVenueLocation(wearable, date = dates[1][2], venueDesc = "SSR1")
		addPointsForVenueLocation(wearable, date = dates[1][3].nextTimestamp(900), venueDesc = "T1",  numberOfPoints = 90)

		addPointsForVenueLocation(wearable, date = dates[2][1], venueDesc = "BSR2")
		addPointsForVenueLocation(wearable, date = dates[2][1].nextTimestamp(900), venueDesc = "T1",  numberOfPoints = 90)

		addPointsForVenueLocation(wearable, date = dates[3][1], venueDesc = "BSR2")
		addPointsForVenueLocation(wearable, date = dates[3][1].nextTimestamp(900), venueDesc = "T1",  numberOfPoints = 90)

		addPointsForVenueLocation(wearable, date = dates[4][1], venueDesc = "SSR3")
		addPointsForVenueLocation(wearable, date = dates[4][1].nextTimestamp(900), venueDesc = "T1",  numberOfPoints = 90)
	}

	private fun createHighTrajectory12() {
		/* infected trajectory 1 Mon -> Fri
		 *
		 * M: 8 BSR1 10 T1 12 SSR1
		 * T: 8 BSR1 10 T1 12 SSR1 14 T1
		 * W: 8 BSR2
		 * T: 8 BSR2
		 * F:
		 *
		 */

		val wearable = WearableEntity(wearableId = 30000012, riskLabel = RiskLabel.HIGH, labelType = LabelType.EDGE)
		wearableRepository.save(wearable)

		addPointsForVenueLocation(wearable, date = dates[0][1], venueDesc = "BSR1")
		addPointsForVenueLocation(wearable, date = dates[0][1].nextTimestamp(900), venueDesc = "T1", numberOfPoints = 90)
		addPointsForVenueLocation(wearable, date = dates[0][3], venueDesc = "SSR1")

		addPointsForVenueLocation(wearable, date = dates[1][1], venueDesc = "BSR1")
		addPointsForVenueLocation(wearable, date = dates[1][1].nextTimestamp(900), venueDesc = "T1",  numberOfPoints = 90)
		addPointsForVenueLocation(wearable, date = dates[1][2], venueDesc = "SSR1")
		addPointsForVenueLocation(wearable, date = dates[1][3].nextTimestamp(900), venueDesc = "T1",  numberOfPoints = 90)

		addPointsForVenueLocation(wearable, date = dates[2][1], venueDesc = "BSR2")
		addPointsForVenueLocation(wearable, date = dates[2][1].nextTimestamp(900), venueDesc = "T1",  numberOfPoints = 90)

		addPointsForVenueLocation(wearable, date = dates[3][1], venueDesc = "BSR2")

		addPointsForVenueLocation(wearable, date = dates[4][1], venueDesc = "SSR3")
	}

	private fun createHighTrajectory13() {
		/* infected trajectory 1 Mon -> Fri
		 *
		 * M: 8 BSR1 10 T1 12 SSR1
		 * T: 8 BSR1 10 T1 12 SSR1 14 T1
		 * W: 8 BSR2 10 T1
		 * T: 8 BSR2 10 T1
		 * F:
		 *
		 */

		val wearable = WearableEntity(wearableId = 30000013, riskLabel = RiskLabel.HIGH, labelType = LabelType.EDGE)
		wearableRepository.save(wearable)

		addPointsForVenueLocation(wearable, date = dates[0][1], venueDesc = "BSR1")
		addPointsForVenueLocation(wearable, date = dates[0][1].nextTimestamp(900), venueDesc = "T1", numberOfPoints = 90)
		addPointsForVenueLocation(wearable, date = dates[0][3], venueDesc = "SSR1")

		addPointsForVenueLocation(wearable, date = dates[1][1], venueDesc = "BSR1")
		addPointsForVenueLocation(wearable, date = dates[1][1].nextTimestamp(900), venueDesc = "T1",  numberOfPoints = 90)
		addPointsForVenueLocation(wearable, date = dates[1][2], venueDesc = "SSR1")
		addPointsForVenueLocation(wearable, date = dates[1][3].nextTimestamp(900), venueDesc = "T1",  numberOfPoints = 90)

		addPointsForVenueLocation(wearable, date = dates[2][1], venueDesc = "BSR2")
		addPointsForVenueLocation(wearable, date = dates[2][1].nextTimestamp(900), venueDesc = "T1",  numberOfPoints = 90)

		addPointsForVenueLocation(wearable, date = dates[3][1], venueDesc = "BSR2")
		addPointsForVenueLocation(wearable, date = dates[3][1].nextTimestamp(900), venueDesc = "T1",  numberOfPoints = 90)

	}

	private fun createHighTrajectory14() {
		/* infected trajectory 1 Mon -> Fri
		 *
		 * M: 14 SSR1
		 * T: 14 SSR1
		 * W:
		 * T: 14 SSR3
		 * F: 10 SSR3
		 *
		 */

		//region data chunks
		val wearable = WearableEntity(wearableId = 30000014, riskLabel = RiskLabel.HIGH)
		wearableRepository.save(wearable)

		addPointsForVenueLocation(wearable, date = dates[0][3], venueDesc = "SSR1")

		addPointsForVenueLocation(wearable, date = dates[1][3], venueDesc = "SSR1")

		addPointsForVenueLocation(wearable, date = dates[3][3], venueDesc = "SSR3")

		addPointsForVenueLocation(wearable, date = dates[4][1], venueDesc = "SSR3")
		//endregion
	}

	private fun createHighTrajectory15() {
		/* infected trajectory 1 Mon -> Fri
		 *
		 * M: 14 SSR1
		 * T: 14 SSR1 16 SSR2
		 * W:
		 * T: 8 BSR2
		 * F:
		 *
		 */

		//region data chunks
		val wearable = WearableEntity(wearableId = 30000015, riskLabel = RiskLabel.HIGH)
		wearableRepository.save(wearable)

		addPointsForVenueLocation(wearable, date = dates[0][3], venueDesc = "SSR1")

		addPointsForVenueLocation(wearable, date = dates[1][3], venueDesc = "SSR1")
		addPointsForVenueLocation(wearable, date = dates[1][4], venueDesc = "SSR2")


		addPointsForVenueLocation(wearable, date = dates[3][2], venueDesc = "SSR3")

		//endregion
	}
	//endregion


	private fun createTrainingHighTrajectory1() {
		/* low trajectory 1 Mon -> Fri
		 *
		 * M: 8 BSR1
		 *
		 */

		val wearable = WearableEntity(wearableId = 40000001, riskLabel = RiskLabel.TRAINING_HIGH_BSR)
		wearableRepository.save(wearable)

		addPointsForVenueLocation(wearable, date = dates[0][0], venueDesc = "BSR1")
	}

	private fun createTrainingHighTrajectory2() {
		/* infected trajectory 1 Mon -> Fri
		 *
		 * M: 12 SSR1
		 * T: 12 SSR1 16 SSR2
		 * W:
		 * T:
		 * F:
		 *
		 */

		val wearable = WearableEntity(wearableId = 40000002, riskLabel = RiskLabel.TRAINING_HIGH_SSR)
		wearableRepository.save(wearable)

		addPointsForVenueLocation(wearable, date = dates[0][2], venueDesc = "SSR1")

	}

	private fun createTrainingHighTrajectory3() {
		/* low trajectory 9 Mon -> Fri
		 *
		 * M: 10 T1
		 *
		 */

		val wearable = WearableEntity(wearableId = 40000003, riskLabel = RiskLabel.TRAINING_HIGH_T)
		wearableRepository.save(wearable)

		addPointsForVenueLocation(wearable, date = dates[0][1], venueDesc = "T1", numberOfPoints = 90)
	}

	private fun createTrainingHighTrajectory4() {
		/* low trajectory 1 Mon -> Fri
		 *
		 * M: 8 BSR1
		 *
		 */

		val wearable = WearableEntity(wearableId = 40000004, riskLabel = RiskLabel.TRAINING_HIGH_BSR_N)
		wearableRepository.save(wearable)

		addPointsForVenueLocation(wearable, date = dates[0][1], venueDesc = "BSR1")
	}

	private fun createTrainingHighTrajectory5() {
		/* infected trajectory 1 Mon -> Fri
		 *
		 * M: 12 SSR1
		 * T: 12 SSR1 16 SSR2
		 * W:
		 * T:
		 * F:
		 *
		 */

		val wearable = WearableEntity(wearableId = 40000005, riskLabel = RiskLabel.TRAINING_HIGH_SSR_N)
		wearableRepository.save(wearable)

		addPointsForVenueLocation(wearable, date = dates[0][3], venueDesc = "SSR1")

	}

	private fun createTrainingHighTrajectory6() {
		/* low trajectory 9 Mon -> Fri
		 *
		 * M: 10 T1
		 *
		 */

		val wearable = WearableEntity(wearableId = 40000006, riskLabel = RiskLabel.TRAINING_HIGH_T_N)
		wearableRepository.save(wearable)

		addPointsForVenueLocation(wearable, date = dates[0][1].nextTimestamp(900), venueDesc = "T1", numberOfPoints = 90)
	}

}
