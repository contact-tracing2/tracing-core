package de.uniba.akshaychoudhry.tracingcore.dao.entity

import lombok.EqualsAndHashCode
import lombok.ToString
import java.util.*
import javax.persistence.*
import javax.persistence.GenerationType.*

@Entity
@Table(name = "wearable_position_master", indexes = [Index(
	name = "trajectory_index",
	columnList = "wearable_id, timestamp"
)])
data class WearableLocationEntity (@Id @GeneratedValue(strategy = AUTO) var id:Long? = null,

								   @ToString.Exclude
								   @EqualsAndHashCode.Exclude
								   @ManyToOne @JoinColumn(name = "wearable_id") var wearable: WearableEntity,
								   @Temporal(TemporalType.TIMESTAMP)var timestamp: Date,
								   @ManyToOne @JoinColumn(name = "venue_id") var venue: VenueEntity)