package de.uniba.akshaychoudhry.tracingcore.dao.repository

import de.uniba.akshaychoudhry.tracingcore.dao.entity.*
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface VenueRepository:CrudRepository<VenueEntity,Long> {
	fun findAllBy():Iterable<VenueEntity> //find -> eager
}

@Repository
interface WearableLocationRepository:CrudRepository<WearableLocationEntity,Long> {
	fun findByWearableOrderByTimestampAsc(wearable: WearableEntity):Iterable<WearableLocationEntity> //get -> lazy | exceptions thrown
	fun findAllByWearable_WearableIdOrderByTimestampAsc(wearableId: Int): Iterable<WearableLocationEntity>
}

@Repository
interface WearableRepository:CrudRepository<WearableEntity,Long> {
	fun findByRiskLabel(riskLabel: RiskLabel):Iterable<WearableEntity>
	fun findByWearableId(wearableId: Int):WearableEntity
	fun findAllBy():Iterable<WearableEntity>
}

@Repository
interface ResultRepository:CrudRepository<ResultEntity,Long>

@Repository
interface StayPointRepository:CrudRepository<StayPointEntity,Long>
