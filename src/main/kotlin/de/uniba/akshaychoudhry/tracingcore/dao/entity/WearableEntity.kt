package de.uniba.akshaychoudhry.tracingcore.dao.entity

import lombok.EqualsAndHashCode
import lombok.ToString
import javax.persistence.*

@Entity
@Table(name = "wearable_master")
data class WearableEntity(@Id @GeneratedValue(strategy = GenerationType.AUTO) var id:Long? = null,
						  var wearableId: Int,
						  var riskLabel: RiskLabel,
						  var labelType: LabelType = LabelType.BULK,

						  @ToString.Exclude
						  @EqualsAndHashCode.Exclude
						  @Column(nullable = true) @OneToMany(fetch = FetchType.LAZY, mappedBy = "wearable", cascade = [CascadeType.ALL], orphanRemoval = true)
						  var wearableLocations:List<WearableLocationEntity>? = null,

						  @ToString.Exclude
						  @EqualsAndHashCode.Exclude
						  @Column(nullable = true) @OneToMany(fetch = FetchType.LAZY, mappedBy = "wearable", cascade = [CascadeType.ALL], orphanRemoval = true)
						  var results:List<ResultEntity>? = null)

enum class RiskLabel {
	LOW, HIGH, POSITIVE, UNKNOWN, TRAINING_HIGH_BSR, TRAINING_HIGH_SSR, TRAINING_HIGH_T, TRAINING_HIGH_BSR_N, TRAINING_HIGH_SSR_N, TRAINING_HIGH_T_N
}

enum class LabelType {
	BULK, EDGE
}