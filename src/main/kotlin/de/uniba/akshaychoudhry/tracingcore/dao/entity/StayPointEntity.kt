package de.uniba.akshaychoudhry.tracingcore.dao.entity

import java.util.*
import javax.persistence.*

@Entity
@Table(name = "stay_points_master")
data class StayPointEntity (@Id @GeneratedValue(strategy = GenerationType.AUTO) var id:Long? = null,
							@Temporal(TemporalType.TIMESTAMP) var start: Date,
							@Temporal(TemporalType.TIMESTAMP) var end: Date,
							@ManyToOne @JoinColumn(name = "venue_id") var venue: VenueEntity)