package de.uniba.akshaychoudhry.tracingcore.dao.entity

import lombok.EqualsAndHashCode
import lombok.ToString
import javax.persistence.*

@Entity
@Table(name = "result_master")
data class ResultEntity(@Id @GeneratedValue(strategy = GenerationType.AUTO) var id:Long? = null,

						@ToString.Exclude
						@EqualsAndHashCode.Exclude
						@ManyToOne @JoinColumn(name = "wearable_id") var wearable: WearableEntity,
						var score:Double,
						var riskLabel: RiskLabel)
