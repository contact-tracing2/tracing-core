package de.uniba.akshaychoudhry.tracingcore.dao.entity

import javax.persistence.*
import javax.persistence.GenerationType.*

@Entity
@Table(name = "venue_master")
data class VenueEntity(@Id @GeneratedValue(strategy = AUTO) var id:Long? = null,
					   var venueSize:Double,
					   var venueHeight:Double = 2.0,
					   var venueDesc:String,
					   var venueLongDesc:String,

					   @Column(nullable = true) @OneToMany (fetch = FetchType.LAZY, mappedBy = "venue", cascade = [CascadeType.ALL], orphanRemoval = true)
					   var wearableLocations:List<WearableLocationEntity>? = null,

					   @Column(nullable = true) @OneToMany(fetch = FetchType.LAZY, mappedBy = "venue", cascade = [CascadeType.ALL], orphanRemoval = true)
						var stayPoints:List<StayPointEntity>? = null)