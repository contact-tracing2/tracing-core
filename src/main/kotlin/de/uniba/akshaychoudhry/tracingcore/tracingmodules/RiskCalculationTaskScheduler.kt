package de.uniba.akshaychoudhry.tracingcore.tracingmodules

import de.uniba.akshaychoudhry.tracingcore.dao.entity.ResultEntity
import de.uniba.akshaychoudhry.tracingcore.dao.entity.RiskLabel
import de.uniba.akshaychoudhry.tracingcore.dao.entity.WearableEntity
import de.uniba.akshaychoudhry.tracingcore.dao.entity.WearableLocationEntity
import de.uniba.akshaychoudhry.tracingcore.dao.repository.WearableLocationRepository
import de.uniba.akshaychoudhry.tracingcore.dao.repository.WearableRepository
import de.uniba.akshaychoudhry.tracingcore.utilities.AerosolSubcostExtrapolator
import de.uniba.akshaychoudhry.tracingcore.utilities.NUMBER_OF_THREADS
import de.uniba.akshaychoudhry.tracingcore.utilities.ScaleFunction
import de.uniba.akshaychoudhry.tracingcore.utilities.all
import lombok.extern.slf4j.Slf4j
import java.util.*
import java.util.concurrent.CompletableFuture
import java.util.concurrent.Executors
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

@Slf4j
class RiskCalculationTaskScheduler(
    private val idToWearableMap: HashMap<Int, WearableEntity>,
    private val contagiousStayPointsMap: HashMap<String,MutableList<Pair<Date, Date>>>,
    private val infectedTrajectory: Iterable<WearableLocationEntity>,
    private val wearableRepository: WearableRepository,
    private val wearableLocationRepository: WearableLocationRepository) {
    private val executor = Executors.newFixedThreadPool(NUMBER_OF_THREADS)

    fun executeMultiThreadedContactTracing(ScaleFunction: ScaleFunction,
                                           aerosolSubcostExtrapolator: AerosolSubcostExtrapolator): List<ResultEntity> {

        val completableResultEntityFutureList = ArrayList<CompletableFuture<ResultEntity>>()
        val scaledSubcostCalculationModule = ScaledSubcostCalculationModule(ScaleFunction,aerosolSubcostExtrapolator)

        idToWearableMap.keys.forEach { wearableId ->
            //fetch a trajectory
            if (!idToWearableMap.containsKey(wearableId)) {		//If a trajectory was not found for wearable_id
                println("MISSING TRAJECTORY FOR KEY: $wearableId")
                return@forEach
            } else if (idToWearableMap[wearableId] == null) {	//If a trajectory was found to be null for wearable_id
                println("NULL TRAJECTORY FOR KEY $wearableId")
                return@forEach
            } else {									//If a valid trajectory was found against the wearable_id
                val completableResultEntityFuture = CompletableFuture<ResultEntity>()

                executor.submit<Any?> {
                    val currentWearable = wearableRepository.findByWearableId(wearableId)
                    val currentTrajectory = wearableLocationRepository.findAllByWearable_WearableIdOrderByTimestampAsc(wearableId)

                    //calculate risk
                    val riskCalculationModule: RiskCalculationModule = SynchronousRiskCalculationModule(contagiousStayPointsMap, scaledSubcostCalculationModule)
                    val calculatedRisk = try {
                        riskCalculationModule.calculateRisk(infectedTrajectory, currentTrajectory)
                    } catch (e: Exception) {
                        println("An Error Occured. $e")
                        throw e
                    }
//                    println("Calculated Risk for wearable $wearableId = ${calculatedRisk.risk}")

                    val resultEntity = ResultEntity(wearable = currentWearable, score = calculatedRisk.risk, riskLabel = RiskLabel.UNKNOWN)
                    completableResultEntityFuture.complete(resultEntity)
                }
                completableResultEntityFutureList.add(completableResultEntityFuture)
            }
        }
        val completableResultEntityListFuture = all(completableResultEntityFutureList) //blocking call
        executor.shutdown()
        return completableResultEntityListFuture.get()
    }

}