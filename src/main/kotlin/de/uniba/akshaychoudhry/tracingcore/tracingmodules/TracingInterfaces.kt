package de.uniba.akshaychoudhry.tracingcore.tracingmodules

import de.uniba.akshaychoudhry.tracingcore.dao.entity.WearableLocationEntity
import de.uniba.akshaychoudhry.tracingcore.utilities.AerosolSubcostExtrapolator
import de.uniba.akshaychoudhry.tracingcore.utilities.ScaleFunction
import java.util.*
import java.util.concurrent.CompletableFuture


abstract class SubcostCalculationModule(val scaleFunction: ScaleFunction, val aerosolSubcostExtrapolator: AerosolSubcostExtrapolator) {
    abstract fun getSubcost(wearableLocationEntity: WearableLocationEntity,
                   contagiousStayPointsMap: HashMap<String, MutableList<Pair<Date, Date>>>): Double
}

open class RiskScore internal constructor(open val risk:Double)

abstract class RiskCalculationModule(private val contagiousStayPointsMap:HashMap<String,MutableList<Pair<Date,Date>>>,
                                     private val subcostCalculationModule: SubcostCalculationModule
) {
    abstract fun prepare()
    abstract fun calculateRisk(infectedTrajectory: Iterable<WearableLocationEntity>,
                               currentTrajectory: Iterable<WearableLocationEntity>,
                               depth: Int = 0): RiskScore
}
