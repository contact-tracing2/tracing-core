package de.uniba.akshaychoudhry.tracingcore.tracingmodules

import de.uniba.akshaychoudhry.tracingcore.dao.entity.WearableLocationEntity
import de.uniba.akshaychoudhry.tracingcore.utilities.*
import java.util.*
import kotlin.collections.HashMap

class ScaledSubcostCalculationModule(
    scaleFunction: ScaleFunction,
    aerosolSubcostExtrapolator: AerosolSubcostExtrapolator
) : SubcostCalculationModule(scaleFunction,aerosolSubcostExtrapolator) {
    private val subcostMap = HashMap<String,Double>()


    override fun getSubcost(wearableLocationEntity: WearableLocationEntity,
                            contagiousStayPointsMap: java.util.HashMap<String, MutableList<Pair<Date, Date>>>): Double {
        val venue = wearableLocationEntity.venue.venueDesc
        val timestamp = wearableLocationEntity.timestamp
        if (!subcostMap.containsKey("$venue#$timestamp")) {
            subcostMap["$venue#$timestamp"] = extrapolateSubcost(timestamp,venue,contagiousStayPointsMap)
        }
        return scaleFunction(subcostMap["$venue#$timestamp"]!!, wearableLocationEntity.venue.venueSize, wearableLocationEntity.venue.venueHeight) / SUBCOST_CORRECTION_FACTOR
    }

    private fun extrapolateSubcost(timestamp: Date, venueDesc: String, contagiousStayPointsMap: java.util.HashMap<String, MutableList<Pair<Date, Date>>>): Double {
        if (contagiousStayPointsMap.containsKey(venueDesc)) {
            contagiousStayPointsMap[venueDesc]!!.forEach {
                when {
                    it.first.time <= timestamp.time && timestamp.time <= it.second.time -> {
                        return (timestamp.subtract(it.first)
                            .toDouble() / SUBCOST_ALTERATION_INTERVAL_MILLIS.toDouble())
                    }
                    it.second.time < timestamp.time && timestamp.subtract(it.second) <= MAX_CONTAGIOUS_INTERVAL_MILLIS -> {
                        return aerosolSubcostExtrapolator(it, timestamp) / SUBCOST_ALTERATION_INTERVAL_MILLIS.toDouble()
                    }
                }
            }
            println("Illegal stage occurred. TS $timestamp not falling in any case.")
            return 0.0
        }
        println("Illegal stage occurred. Venue $venueDesc not in map")
        return 0.0
    }
}

