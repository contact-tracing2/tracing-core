package de.uniba.akshaychoudhry.tracingcore.tracingmodules

import de.uniba.akshaychoudhry.tracingcore.dao.entity.ResultEntity
import de.uniba.akshaychoudhry.tracingcore.dao.entity.WearableLocationEntity
import de.uniba.akshaychoudhry.tracingcore.utilities.MAX_CONTAGIOUS_INTERVAL_MILLIS
import de.uniba.akshaychoudhry.tracingcore.utilities.subtract
import java.util.*
import java.util.concurrent.CompletableFuture
import kotlin.collections.HashMap


class SynchronousRiskCalculationModule2(
    private val contagiousStayPointsMap:HashMap<String,MutableList<Pair<Date, Date>>>,
    private val subcostCalculationModule: SubcostCalculationModule
): RiskCalculationModule(contagiousStayPointsMap, subcostCalculationModule) {

    private var riskScoreMap = HashMap<String,RiskScore>()

    override fun calculateRisk(infectedTrajectory: Iterable<WearableLocationEntity>,
                               currentTrajectory: Iterable<WearableLocationEntity>,
                               depth: Int): RiskScore {
        try {
            if (infectedTrajectory.none() || currentTrajectory.none()) {
                //println("Leaf node with $depth")
                return RiskScore(0.0)    // either of the trajectories is empty
            }

            val ITFirst = infectedTrajectory.first()
            val CTFirst = currentTrajectory.first()

            if (!riskScoreMap.containsKey("${ITFirst.timestamp}_${CTFirst.timestamp}")) { // calculate if not already hashed
                riskScoreMap["${ITFirst.timestamp}_${CTFirst.timestamp}"] = when {
                    ITFirst.timestamp.after(CTFirst.timestamp) -> {				// contagious TS is after current TS -> drop from current TS
                        calculateRisk(
                            infectedTrajectory,
                            currentTrajectory.dropWhile { ITFirst.timestamp.after(it.timestamp) },
                            depth + 1
                        )
                    }
                    ITFirst.timestamp.subtract(CTFirst.timestamp) < -MAX_CONTAGIOUS_INTERVAL_MILLIS -> {	// current TS is after contagious TS by 3 hours or more -> drop from current TS
                        calculateRisk(
                            infectedTrajectory.dropWhile { it.timestamp.subtract(CTFirst.timestamp) < -MAX_CONTAGIOUS_INTERVAL_MILLIS },
                            currentTrajectory,
                            depth + 1
                        )
                    }
                    else -> { // current TS is in the risk time
                        var cost = 0.0
                        var lCost = RiskScore(0.0)
                        var rCost = RiskScore(0.0)

                        cost = calculateRisk(infectedTrajectory.drop(1),
                            currentTrajectory.drop(1),
                            depth + 1).risk + subcostCalculationModule.getSubcost(CTFirst, contagiousStayPointsMap)

                        if (CTFirst.timestamp.after(ITFirst.timestamp)) {
                            lCost = calculateRisk(infectedTrajectory.drop(1), currentTrajectory, depth + 1)
                        }
                        rCost = calculateRisk(infectedTrajectory, currentTrajectory.drop(1), depth + 1)
                        RiskScore((cost).coerceAtLeast(lCost.risk).coerceAtLeast(rCost.risk))
                    }
                }
            }

            return riskScoreMap["${ITFirst.timestamp}_${CTFirst.timestamp}"]!!
        } catch (e: StackOverflowError) {
            println("depth before overflow = $depth")
            return RiskScore(0.0)
        }
    }

    override fun prepare() {
        riskScoreMap = HashMap()
    }

}


class SynchronousRiskCalculationModule(
    private val contagiousStayPointsMap:HashMap<String,MutableList<Pair<Date, Date>>>,
    private val subcostCalculationModule: SubcostCalculationModule
): RiskCalculationModule(contagiousStayPointsMap, subcostCalculationModule) {

    private var riskScoreMap = HashMap<String,RiskScore>()

    override fun calculateRisk(infectedTrajectory: Iterable<WearableLocationEntity>,
                               currentTrajectory: Iterable<WearableLocationEntity>,
                               depth: Int): RiskScore {
        if (currentTrajectory.none()) {
            println("Leaf node with $depth")
            return RiskScore(0.0)    // either of the trajectories is empty
        }

        val ITFirst = infectedTrajectory.first()
        val CTFirst = currentTrajectory.first()
        val wearableId = CTFirst.wearable.wearableId

        var score = 0.0

        if (!riskScoreMap.containsKey("${wearableId}_${CTFirst.timestamp}")) { // calculate if not already hashed
            currentTrajectory.forEach {
                score += subcostCalculationModule.getSubcost(it, contagiousStayPointsMap)
            }
            riskScoreMap["${wearableId}_${CTFirst.timestamp}"] =  RiskScore(score)
        }

        return riskScoreMap["${wearableId}_${CTFirst.timestamp}"]!!
    }

    override fun prepare() {
        riskScoreMap = HashMap()
    }

}